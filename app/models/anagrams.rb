class Anagrams

  def self.discompose_word(a_word)
    result = {}
    a_word.each_char do |char|
      result.keys.include?(char) ? (result[char] = (result[char] + 1)) : (result[char] = 1)
    end
    result
  end

  def self.anagrams(word , possible_anagrams)
    discompose_word = self.discompose_word(word)
    possible_anagrams.select {|a_possibility| self.discompose_word(a_possibility) == discompose_word}
  end

end