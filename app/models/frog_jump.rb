class FrogJump < Object

  def self.frog_jump(x, y, d)
    divmod = (y - x).divmod(d)
  	divmod[0] + (divmod[1] > 0 ? 1 : 0)
  end

end