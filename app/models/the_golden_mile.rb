require 'rgeo/geo_json'
class TheGoldenMile < Object

  def initialize(a_file_name)
    @a_file_name = a_file_name
  end
  
  def optimal_path
   path = Rails.root.join('tmp', 'storage', @a_file_name)
   file = open(path)
   json = file.read
   parsed = JSON.parse(json)
   
   minorPathFor(parsed["features"].collect {|each| RGeo::GeoJSON.decode(each)})
  end
  
  private
  
  def minorPathFor(breweries)
  	possible_paths = breweries.collect {|brewery| minor_path_starting_on(breweries , brewery)}
    (possible_paths.sort { |x,y| x[1] <=> y[1] }).first
  end
  
  def minor_path_starting_on(a_collection_of_brewery, brewery, previous_path = [], previous_distance = 0)
    path= previous_path
    path << brewery
	  possible_destination = Array.new(a_collection_of_brewery)
	  possible_destination.delete(brewery)
	  if possible_destination.any?
	    next_brewery = (possible_destination.sort { |x,y| (distance_beetween(brewery, x)) <=> (distance_beetween(brewery, y))}).first
	    distance = previous_distance + distance_beetween(brewery, next_brewery)
	    
	    minor_path_starting_on(possible_destination, next_brewery, path, distance)
	  else
	    [path.collect {|each| each.properties["name"]}.join(' -> '), "Distancia: #{previous_distance} km" , "Tiempo: #{previous_distance / 60} Horas"]
	  end
  end
  
  def distance_beetween(brewery, another_brewery)
    brewery_location = Geokit::LatLng.new(brewery.geometry.x,brewery.geometry.y)
    destination = "#{another_brewery.geometry.x},#{another_brewery.geometry.y}"
    brewery_location.distance_to(destination) * 1.60934
  end
    
end